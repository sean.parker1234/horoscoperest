package com.Jersey.jarxs.horoscopeREST;

import com.sun.net.httpserver.HttpServer;
import com.sun.jersey.api.container.httpserver.HttpServerFactory;
import java.io.IOException;
import java.sql.*;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

//path hosted at /horoscope
@Path("/horoscope")
public class horoscopeREST {

    //gets start sign from parameter
    @GET
    @Path("/{starSign}")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public String receiveStarSign(@PathParam("starSign") String starSign){
        String horoscope = getHoroscope(starSign);
        System.out.println(horoscope);
        return "Horoscope: " + horoscope;
    }
    public static void main(String[] args) throws IOException {
        HttpServer server = HttpServerFactory.create("http://localhost:9998/");
//        DatabaseManage db = new DatabaseManage();
//        db.createDatabase();
        server.start();

        System.out.println("Server running");
        System.out.println("Visit: http://localhost:9998/");
        System.out.println("Hit return to stop...");
        System.in.read();
        System.out.println("Stopping server");
        server.stop(0);
        System.out.println("Server stopped");
    }

    public String getHoroscope(String starSign){

        String[] starSigns = {"Taurus", "Aries", "Gemini", "Cancer", "Leo", "Virgo", "Libra", "Scorpio", "Sagittarius", "Capricorn", "Aquarius", "Pisces"};
        int index = -1;
        System.out.println(starSign);
        //gets index of star sign to pass to switch statement
        for(int i=0; i < starSigns.length; i++){
            System.out.println(index);
            System.out.println(starSigns[i]);
            if(starSigns[i].equals(starSign))
                index = i;
        }
        //
        String horoscope = getHoroscopeFromIndex(index);
        return  horoscope;
    }

    public String getHoroscopeFromIndex(int index){
        //gives random value between 1 and 3
        int randomHoroscope = (int)(Math.random() * (3 - 1) + 1);
        String choosenHoroscope = "";
        switch(index){
            case 0:
                String Aries1 = "You're loaded with energy right now, and being so high-spirited, you'll have a lot of fun juggling all the tasks that other people find too boring to deal with.";
                String Aries2 = "Even if you're in a sharing mood, you should try to keep all your private things private today.";
                String Aries3 = "You might find that good energy is a bit hard to come by today.";

                String[] AriesList = {Aries1, Aries2, Aries3};

                choosenHoroscope = AriesList[randomHoroscope];
                break;
            case 1:
                String Taurus1 = "If you want to be happy, you've got to make yourself happy.";
                String Taurus2 = "There could be quite a few new faces on the scene today, and you could surprise yourself by reacting a bit too critically to one of them. Try to be polite.";
                String Taurus3 = "today, you need to follow up on any odd behavior. It could be a symptom of a bigger issue. If a friend is acting out of character, don't just assume they woke up on the wrong side of the bed. Ask them what's going on. Chances are they've been waiting to tell you.";

                String[] TaurusList = {Taurus1, Taurus2, Taurus3};
                choosenHoroscope = TaurusList[randomHoroscope];
                break;
            case 2:
                String Gemini1 = "You're about to get a fresh start in an old relationship. ";
                String Gemini2 = "It's time for you to take your turn organizing some sort of outing. It doesn't have to be major, and it doesn't have to involve lots of people.";
                String Gemini3 = "Planning should be fun for you today, especially if you're planning something that has lots of different elements that need to be coordinated. ";

                String[] GeminiList = {Gemini1, Gemini2, Gemini3};
                choosenHoroscope = GeminiList[randomHoroscope];
                break;
            case 3:
                String Cancer1 = "Old things can be new and exciting for you right now. An old flame could come back on the scene and shake up your world.";
                String Cancer2 = "Your reputation is something you should start taking more seriously. Don't worry. It's not suffering right now, but it could use a polish.";
                String Cancer3 = "Today's a great day to shake up your world and step out of your comfortable comfort zone.";

                String[] CancerList = {Cancer1, Cancer2, Cancer3};
                choosenHoroscope = CancerList[randomHoroscope];
                break;
            case 4:
                String Leo1 = "You share common goals with a very important person, and you should let them know that they can count on you.";
                String Leo2 = "Whether you're looking for a new friend, new business connection, or new love, the possibilities abound! Initiate a conversation with someone you've always been a little bit shy around. ";
                String Leo3 = "You're feeling sensitive to the problems of other people. Your empathy is strong, and your thoughts could be distracted by worry.";

                String[] LeoList = {Leo1, Leo2, Leo3};
                choosenHoroscope = LeoList[randomHoroscope];
                break;
            case 5:
                String Virgo1 = "It's going to take a lot of patience to deal with a friend who seems hell-bent on spoiling your fun today. Whether they're miserly, irritable, or just plain grumpy, you don't have to encourage it.";
                String Virgo2 = "One of your friends is behaving badly, and they aren't going to listen to your common-sense advice today. They're in no mood for cold logic. ";
                String Virgo3 = "Being the good friend you are, you usually give a friend the benefit of the doubt if they say something that sounds rude or weird.";

                String[] VirgoList = {Virgo1, Virgo2, Virgo3};
                choosenHoroscope = VirgoList[randomHoroscope];
                break;
            case 6:
                String Libra1 = "To get some insight on your latest problem, find another person you can share your confusion with. ";
                String Libra2 = "Today is sure to be a good day! You're in the midst of a wonderful phase of good energy. Even the grumpiest of people can't ruin your mood.";
                String Libra3 = "It looks like that worry you had was just a false alarm, and now you could finally get the green light. Rest easy and breathe a sigh of relief. ";

                String[] LibraList = {Libra1, Libra2, Libra3};
                choosenHoroscope = LibraList[randomHoroscope];
                break;
            case 7:
                String Scorpio1 = "Your expectations are growing too lofty right now. You're aiming so high that you're bound to be disappointed when reality hits.";
                String Scorpio2 = "Things are going well for you now, but they might not be moving forward quickly enough. ";
                String Scorpio3 = "Before you get involved in anything new today, take some time to make sure you know what you're doing.";

                String[] ScorpioList = {Scorpio1, Scorpio2, Scorpio3};
                choosenHoroscope = ScorpioList[randomHoroscope];
                break;
            case 8:
                String Sagittarius1 = "If you can be more flexible, you can be more powerful. Refusing to budge in your position or move your schedule around closes you off to too many things and can give you a bad reputation. ";
                String Sagittarius2 = "You've been too tough on yourself over the past few weeks, and today you need to cut yourself some slack. ";
                String Sagittarius3 = "You're in a very open-minded phase in which you could do something that isn't necessarily good for you.";

                String[] SagittariusList = {Sagittarius1, Sagittarius2, Sagittarius3};
                choosenHoroscope = SagittariusList[randomHoroscope];
                break;
            case 9:
                String Capricorn1 = "You'll get the greatest amount of satisfaction from encountering unfamiliar topics today.";
                String Capricorn2 = "Feeling confused about a decision you have to make? If you need more clarity in your thinking, you simply need to make time for it. ";
                String Capricorn3 = "All you have to do when faced with a challenge today is concentrate! Keep your eye on the ball and you'll be sure to win. ";

                String[] CapricornList = {Capricorn1, Capricorn2, Capricorn3};
                choosenHoroscope = CapricornList[randomHoroscope];
                break;
            case 10:
                String Aquarius1 = "It's not a good day for moving around. Wherever you try to go, you can expect long lines, bad traffic, or unpleasant conditions. If you can stay close to home, do it. ";
                String Aquarius2 = "Feel free to express yourself honestly and openly today. Go on and show everyone just how unique you are.";
                String Aquarius3 = "There have been more misunderstandings going on in your circle lately, and this needs to come to a halt. ";

                String[] AquariusList = {Aquarius1, Aquarius2, Aquarius3};
                choosenHoroscope = AquariusList[randomHoroscope];
                break;
            case 11:
                String Pisces1 = "Trying to solve this issue over texts is only going to make the problem worse. You can't move on to more good times unless you put an end to arguments and tension.";
                String Pisces2 = "Challenge yourself today. Not only will you rise to any occasion, but you will also exceed beyond your own highest expectations.";
                String Pisces3 = "An issue might pop up around the house today, and if it does you need to make it your first priority. ";

                String[] PiscesList = {Pisces1, Pisces2, Pisces3};
                choosenHoroscope= PiscesList[randomHoroscope];
                break;
            default:
                choosenHoroscope = "No Horoscope could be found";
        }
        System.out.println("At end of case statement: " + choosenHoroscope);
        return choosenHoroscope;


    }


}
